import time

from random import shuffle

from SimpleAppFramework import *
from PointAndClickTrial import *
from CursorStateReceiver import *

import experiment_logger

info = showDialog(
	"Participant Setup",
	{
		"Participant #": None,
#		"Participant Initials": None,
		"Input device": None,
	}
)

if info == None:
	exit()
	
try:
	os.mkdir("data")
except:
	pass

dwellIndicator = CursorStateReceiver()
dwellIndicator.enable()

try:
	replayFilename = "data/replayLog_%s_%s.txt" % (
		info["Participant #"],
#		info["Participant Initials"],
		info["Input device"]
	)
	
	datafilename = "data/FittsLawGame_%s_%s.txt" % (
		info["Participant #"],
#		info["Participant Initials"],
		info["Input device"]
	)
	
	experiment_logger.setLogFile(replayFilename)
	experiment_logger.logEvent("\nlogging started", [replayFilename])
	
	screenDimensions = getScreenRectangle()
	experiment_logger.logEvent("screen-geometry", screenDimensions)
	
	with open(datafilename, "a") as dataFile:
		dataFile.write(
			"\n\nStart:\t%s\tResolution\t%dx%d\n\n" % (
				time.strftime("%Y-%m-%d %H:%M:%S"),
				screenDimensions[2],
				screenDimensions[3]
			)
		)
		dataFile.write(
			"Block\tTrial\tRT\tDotDistance(W)\tDotSize(A)\tDotAngle\t" +
			"MT\tID\tIP\tMissClicks\tDtS\tTargets=>\n"
		)
except:
	print "Could not open data file!"
	exit(1)

experiment_logger.logEvent("instructions", [1])
showInstructionScreen(
	"On the next several screens you will see a white circle followed by a black circle. " +
		"You must position the cursor in each circle and select it to proceed to the next pair. " +
		"You will first go through a practice block followed by several experimental blocks. " +
		"For each experimental trial, you will be scored on your response time. " + 
		"The faster you complete each trial, the higher your score.",
	"This first set of trials is practice.",
	"Press any key to proceed!"
)
time.sleep(1)

factory = TwoTargetFactory(centerStartTarget=False, recenterMouse=False)

# Do practice runs
score = 0

stimulusList = splitFile(basedir + '/stimulationLists/practice.txt', "\n", "\t")
shuffle(stimulusList)
experiment_logger.logEvent("practice-trials")

for parameters in stimulusList:
	distance = int(parameters[0])
	size = int(parameters[1])
	
	trial = factory.createTrial(distance, size)
	trial.setTimeout(10)
	trial.run()

	score += calculateScore(distance, size, trial.getClickToClickTime(), playSound=False, showMessage=False)
	time.sleep(0.5)

experiment_logger.logEvent("practice-score")
showInstructionScreen(
	"Practice Score: %d" % score,
	"Press any key to proceed!"
)
time.sleep(1)

# Do the real runs
hud = HUD()
hud.enable()
blockCount = 10
stimulusList = splitFile(basedir + '/stimulationLists/experimental.txt', "\n", "\t")
for blockID in range(blockCount):
	shuffle(stimulusList)
	
	if blockID != 0:
		experiment_logger.logEvent("start-block-countdown", [blockID+1])
		showCountDown(3, False, "Ready for Block %d" % (blockID+1), "Click in the box below to continue or pause")

	experiment_logger.logEvent("start-block", [blockID+1])
	experiment_logger.flush()
	trialID = 0
	for parameters in stimulusList:
		trialID += 1
		sleep(0.5)
		distance = int(parameters[0])
		size = int(parameters[1])
		
		trial = factory.createTrial(distance, size)
		trial.setTimeout(10)
		experiment_logger.logEvent("start-trial", [blockID+1, trialID, distance, size])
		trial.run()
		trialScore = calculateScore(distance, size, trial.getClickToClickTime(), playSound=False, showMessage=False)
		experiment_logger.logEvent("finish-trial", [trialScore])
		hud.addScore(trialScore)

		with open(datafilename, "a") as dataFile:
			indexOfDifficulty = math.log(2*float(distance)/size, 2)
			reactionTime = trial.getTotalTime()
			if reactionTime == None:
				reactionTime = -1
				
			movementTime = trial.getClickToClickTime()
			if movementTime == None or movementTime == 0:
				indexOfPerformance = -1
				movementTime = -1
			else:
				indexOfPerformance = float(indexOfDifficulty) / movementTime
				
			dts = dwellIndicator.getDwellToSelectTime()
			if dts == None:
				dts = -1

			fieldsToWrite = [
				("%d", (1+blockID)),
				("%d", trialID),
				("%2.8f", reactionTime),
				("%d", distance),
				("%d", size),
				("%2.8f", factory.getLastAngle()),
				("%2.8f", movementTime),
				("%2.8f", indexOfDifficulty),
				("%2.8f", indexOfPerformance),
				("%d", trial.missedSelections),
				("%2.8f", dts)
			]
			for target in trial.targets:
				fieldsToWrite.append(("%s", target))

			for formatString, value in fieldsToWrite:
				dataFile.write((formatString + "\t") % value)
			dataFile.write("\n")
			
		progress = (blockID + float(trialID) / len(stimulusList))/blockCount
		hud.setProgress(progress)
		experiment_logger.logEvent("update-hud-progress", [progress])

		experiment_logger.flush()
		
	hud.disable()
	experiment_logger.logEvent("score-summary", [hud.blockScore, hud.overallScore])
	showTimedMessage(2, "Block Score: %d" % hud.blockScore, "Total Score: %d" % hud.overallScore)
	hud.blockScore = 0
	hud.enable()

experiment_logger.logEvent("complete")
showTimedMessage(
	30,
	"You have reached the end of the experiment.",
	"Thank you for your participation."
)

experiment_logger.flush()
