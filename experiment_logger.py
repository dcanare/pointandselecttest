import time

logFile = "log.log"
logBuffer = []
lastMouse = None

def setLogFile(newLogFile):
	global logFile
	
	logFile = newLogFile

def flush():
	global logBuffer

	try:
		with open(logFile, "a") as dataFile:
			for time, eventName, data in logBuffer:
				dataFile.write("%f\t%s\t" % (time, eventName))
				dataFile.write("\t".join(str(x) for x in data))
				dataFile.write("\n")
		logBuffer = []
	except:
		print "FAILED TO FLUSH LOG TO FILE!"

def logEvent(eventName, data=[]):
	logBuffer.append((time.time(), eventName, list(data)))

def logMouse(action, pos, onlyIfNew=True):
	global lastMouse
	
	outputData = list(pos)
	
	outputData.append(action) # add the action just for comparison
	if lastMouse != None:
		if lastMouse == outputData:
			return
			
	lastMouse = list(outputData)
	outputData.pop() # remove the action
	
	logEvent(action, outputData)
