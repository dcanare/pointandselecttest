import time, math, sys, os
from random import seed

import pygame
import pymouse
from pygame.locals import *

import experiment_logger

if getattr(sys, 'frozen', None):
	basedir = sys._MEIPASS
else:
	basedir = os.path.dirname(__file__)

pygame.init()
seed()

screen = None
defaultFont = pygame.font.SysFont("sans", 24)
currentFont = defaultFont
currentColor = (255, 255, 255)
strokeWidth = 0

data,mask = pygame.cursors.compile((
	"        -       ",
	"       X-       ",
	"       X-       ",
	"       X-       ",
	"       X-       ",
	"       X-       ",
	"       X        ",
	" ------   ------",
	"XXXXXX   XXXXXX ",
	"        -       ",
	"       X-       ",
	"       X-       ",
	"       X-       ",
	"       X-       ",
	"       X-       ",
	"       X        ",
), black='X', white='-')
pygame.mouse.set_cursor((16, 16), (8, 8), data, mask)


class Stopwatch(object):
	def __init__(self):
		self.startTime = None

	def start(self):
		self.laps = []
		self.startTime = time.time()

	def stop(self):
		if self.startTime == None:
			raise Exception("Stopwatch not started!")
			
		self.lap()
		self.startTime = None

	def lap(self):
		if self.startTime == None:
			raise Exception("Stopwatch not started!")
			
		currentTime = time.time()
		self.laps.append(currentTime - self.startTime)
		self.startTime = currentTime
		
		return self.laps[len(self.laps) - 1]

	def getTotalTime(self, includeRunning=True):
		total = sum(self.laps)
		if includeRunning and self.startTime != None:
			total = total + (time.time() - self.startTime)
			
		return total

	def isStarted(self):
		return self.isRunning() or len(self.laps) > 0

	def isRunning(self):
		return self.startTime != None

class Scenario(object):
	def init(self):
		if not hasattr(self, 'timeout'):
			self.setTimeout(None)
		
		self.eventMap = {
			KEYUP: self._onKeyUp,
			KEYDOWN: self._onKeyDown,
			MOUSEMOTION: self._onMouseMove,
			MOUSEBUTTONUP: self._onMouseUp,
			MOUSEBUTTONDOWN: self._onMouseDown
		}
		self.done = False
		self.startTime = None

	def run(self):
		global screen
		
		if not hasattr(self, 'done'):
			self.init()
			
		if screen == None:
			print "Creating screen"
			createScreen()

		while not self.isDone():
			currentTime = time.time()

			if self.timeout != None and self.startTime != None:
				if currentTime - self.startTime > self.timeout:
					self.onTimeout()
					
			self._update()
			
			screen.fill((0, 0, 0))
			if not self.isDone():
				self.render()
				
			ScreenOverlay.renderOverlays()
			pygame.display.flip()
			
			time.sleep(.016)	# approx. 60 fps

	def startTimeoutClock(self):
		self.startTime = time.time()

	def _update(self):
		for event in pygame.event.get():
			if event.type in self.eventMap:
				self.eventMap[event.type](event)

			if event.type == KEYDOWN and event.key == K_F4:
				experiment_logger.flush()
				exit(1)
				
		self.update()

	def setTimeout(self, time):
		self.timeout = time

	def isDone(self):
		return self.done

	def _onKeyUp(self, event):
		experiment_logger.logEvent("key-up", (event.key,))
		self.onKeyUp(event)
		
	def _onKeyDown(self, event): 
		experiment_logger.logEvent("key-down", (event.key,))
		self.onKeyDown(event)
		
	def _onMouseMove(self, event): 
		experiment_logger.logMouse("mouse-move", event.pos)
		self.onMouseMove(event)
		
	def _onMouseUp(self, event): 
		experiment_logger.logMouse("mouse-up", event.pos)
		self.onMouseUp(event)
		
	def _onMouseDown(self, event): 
		experiment_logger.logMouse("mouse-down", event.pos)
		self.onMouseDown(event)


	def update(self): pass
	def render(self): pass

	def onKeyUp(self, event): pass
	def onKeyDown(self, event): pass
	def onMouseMove(self, event): pass
	def onMouseUp(self, event): pass
	def onMouseDown(self, event): pass
	def onTimeout(self): pass


class InstructionScenario(Scenario):
	def __init__(self, paragraphs, okKey=None, listenForMouse=False):
		self.init()
		self.text = paragraphs
		self.okKey = okKey
		self.listenForMouse = listenForMouse

	def render(self):
		screenSize = getScreenRectangle()
		wrapText(self.text, 200, 200, screenSize[2]-400, screenSize[3]-400, align="center")

	def onKeyUp(self, event):
		if self.okKey == None or event.key == ord(self.okKey.lower()):
			self.done = True

	def onMouseDown(self, event):
		if self.listenForMouse:
			self.done = True

class TimedMessageScenario(Scenario):
	def __init__(self, paragraphs, timeout, showCountdown=False, startImmediately=True):
		self.init()
		self.setTimeout(timeout)
		self.text = paragraphs
		self.showCountdown = showCountdown
		self.startImmediately = startImmediately
		
	def run(self):
		if self.startImmediately:
			self.startTimeoutClock()
			
		super(TimedMessageScenario, self).run()

	def render(self):
		if self.showCountdown:
			if self.startTime == None:
				wrapText("Paused", 0, 230, align="center")
			else:
				timeRemaining = str(1 + int(self.timeout - (time.time() - self.startTime)))
				wrapText(timeRemaining, 0, 230, align="center")
				
		wrapText(self.text, 0, 200, align="center")

	def onTimeout(self):
		self.done = True

def showInstructionScreen(*instructions):
	instructionScreen = InstructionScenario(instructions)
	instructionScreen.run()

def showTimedMessage(time, *messages):
	messageScreen = TimedMessageScenario(messages, time)
	messageScreen.run()
	
	
class CountdownScenario(TimedMessageScenario):
	def __init__(self, messages, duration, startImmediately=True):
		super(CountdownScenario, self).__init__(messages, duration, True, startImmediately)
		screenSize = getScreenRectangle()
		self.rect = Rectangle(
			.5 * screenSize[2] - 200,
			.5 * screenSize[3] - 200,
			400, 200
		)
		
	def render(self):
		super(CountdownScenario, self).render()
		
		self.rect.render()

	def togglePause(self):
		if self.startTime == None:
			experiment_logger.logEvent("unpause")

			self.startTimeoutClock()
			playAudio(basedir + "/audio/highTone.wav")
		else:
			experiment_logger.logEvent("pause")
			self.startTime = None
			playAudio(basedir + "/audio/lowTone.wav")

		
	def onKeyDown(self, event):
		self.togglePause()
	
	def onMouseDown(self, event):
		if self.rect.contains(event.pos):
			self.togglePause()
	
def showCountDown(time, startImmediately, *messages):
	messageScreen = CountdownScenario(messages, time, startImmediately)
	messageScreen.run()


try:
	from PySide import QtCore
	from PySide import QtGui
	from PySide.QtCore import *
	from PySide.QtGui import *
	class SimpleInputDialog(QWidget):
		def __init__(self, title, fieldList):
			super(SimpleInputDialog, self).__init__()
			
			self.ok = False
			
			self.setWindowTitle(title)
			
			self.fields = {}
			form = QFormLayout()
			for field, typeDetails in fieldList.items():
				self.fields[field] = QtGui.QLineEdit()
				form.addRow(field, self.fields[field])
				
			okButton = QPushButton("Start")
			okButton.clicked.connect(self.okPressed)
			form.addRow("", okButton)
			self.setLayout(form)
			
		def okPressed(self):
			self.ok = True
			self.close()
except:
	pass

def showDialog(title, fieldList):
	values = {}
	try:
		raise("no gui for you-ie")
		app = QApplication([])
		dialog = SimpleInputDialog(title, fieldList)
		dialog.show()
		app.exec_()
		
		if dialog.ok:
			for field, widget in dialog.fields.items():
				values[field] = widget.text()
				
			return values
	except:
		print "\n=== %s ===\n" % title
		for field,default in fieldList.items():
			print field, ":",
			values[field] = raw_input()

		return values
	


def setTitle(title):
	pygame.display.set_caption(title)


def getScreenRectangle():
	infoObject = pygame.display.Info()
	return [0, 0, infoObject.current_w, infoObject.current_h]

def createScreen(size=None, fullscreen=True):
	global screen
	
	if size == None:
		rect = getScreenRectangle()
		size = (rect[2], rect[3])
		
	flags = pygame.DOUBLEBUF|pygame.HWSURFACE
	
	if fullscreen:
		#flags = flags|pygame.FULLSCREEN
		flags = flags|pygame.NOFRAME
		os.environ['SDL_VIDEO_WINDOW_POS'] = "0,0"
		
		
	screen = pygame.display.set_mode(size, flags)


def sleep(sleepTime):
	messageScreen = TimedMessageScenario(None, sleepTime)
	messageScreen.run()

def splitString(string, delimiters, delimiter=None):
	if delimiter == None: delimiter = delimiters.pop(0)
	
	data = string.split(delimiter)
	
	if len(delimiters) > 0:
		delimiter = delimiters.pop(0)
		for i in range(len(data)):
			data[i] = splitString(data[i], delimiters, delimiter)
	
	return data

def splitFile(filename, *delimiters):
	with open(filename, "r") as myFile:
		data = splitString(myFile.read().strip(), list(delimiters))
	
	return data


def playAudio(filename):
	pygame.mixer.music.load(filename)
	pygame.mixer.music.play()





class Rectangle(pygame.Rect):
	def __init__(self, x, y, w, h, color=None, strokeWidth=None):
		super(Rectangle, self).__init__(x, y, w, h)
		self.color = color
		self.strokeWidth = strokeWidth

	def render(self):
		global currentColor, strokeWidth
		color = self.color if self.color != None else currentColor
		strokeWidth = self.strokeWidth if self.strokeWidth != None else strokeWidth

		pygame.draw.rect(screen, color, self, strokeWidth)

	def contains(self, pos):
		return self.collidepoint(pos)

class Square(Rectangle):
	def __init__(self, x, y, w, color=None, strokeWidth=None):
		super(Square, self).__init__(x, y, w, w, color, strokeWidth)

class Ellipse(Rectangle):
	def __init__(self, x, y, w, h, color=None, strokeWidth=None):
		super(Rectangle, self).__init__(x, y, w, h, color, strokeWidth)
		
	def render(self):
		global currentColor, strokeWidth
		color = self.color if self.color != None else currentColor
		strokeWidth = self.strokeWidth if self.strokeWidth != None else strokeWidth
		
		pygame.draw.ellipse(screen, color, self, strokeWidth)

class Circle(object):
	def __init__(self, x, y, r, color=None, strokeWidth=None):
		self.pos = (int(x), int(y))
		self.radius = r
		self.color = color
		self.strokeWidth = strokeWidth
		
	def contains(self, pos):
		return self.radius >= math.sqrt(math.pow(pos[0] - self.pos[0], 2) + math.pow(pos[1] - self.pos[1], 2))

	def render(self):
		global currentColor, strokeWidth
		color = self.color if self.color != None else currentColor
		drawingStrokeWidth = self.strokeWidth if self.strokeWidth != None else strokeWidth

		pygame.draw.circle(screen, color, self.pos, int(self.radius), int(drawingStrokeWidth))

	def __repr__(self):
		return "%d,%d r=%d" % (self.pos[0], self.pos[1], self.radius)

def draw(primitive):
	primitive.render()

class WrappedTextInfo(object):
	def __init__(self, text, maxWidth=None, maxHeight=None, font=None):
		global currentFont
	
		if font == None: font = currentFont
		if maxWidth == None: maxWidth = self.screen.get_width()
		if maxHeight == None: maxHeight = self.screen.get_height()

		if isinstance(text, basestring):
			text = text.strip().split("\\n")

		lines = []
		for p in text:
			start = 0
			while start < len(p):
				# iterate one letter at a time until we spill over
				stop = start + 1
				while font.size(p[start:stop])[0] < maxWidth and stop < len(p):
					stop += 1
					
				if stop < len(p):
					stop = p.rfind(" ", start, stop) + 1
				lines.append(p[start:stop])
				start = stop
			lines.append("")
		lines.pop()

		lineHeight = int(font.size("Tg")[1] * 1.1)	
		
		self.lines = lines
		self.lastX = font.size(lines[-1])[0]
		self.lastY = len(lines)*lineHeight

def renderText(text, x, y, font=None):
	global screen, currentFont, currentColor

	if font == None: font = currentFont
	
	screen.blit(font.render(text, 1, currentColor), (x, y))
	size = font.size(text)
	return [size[0] + x, y]
	
def wrapText(text, x, y, maxWidth=None, maxHeight=None, font=None, align="left"):
	global screen, currentFont
	
	if text == None: return
	
	if font == None: font = currentFont
	if maxWidth == None: maxWidth = screen.get_width() - x
	if maxHeight == None: maxHeight = screen.get_height() - y
	
	wrapData = WrappedTextInfo(text, maxWidth, maxHeight, font)
	lineHeight = int(font.size("Tg")[1] * 1.1)	

	lines = wrapData.lines
	for line in range(len(lines)):
		if align == "left":
			offset = 0
		else:
			lineWidth = font.size(lines[line])[0]
			if align == "right":
				offset = maxWidth - lineWidth
			elif align == "center":
				offset = (maxWidth - lineWidth)/2
				
		renderText(lines[line], x + offset, y + lineHeight * line, font)
		
	return [wrapData.lastX, wrapData.lastY]


class ScreenOverlay(object):
	overlays = []

	def enable(self):
		if not self in ScreenOverlay.overlays:
			ScreenOverlay.overlays.append(self)
		
	def disable(self):
		if self in ScreenOverlay.overlays:
			ScreenOverlay.overlays.remove(self);
		
	def render(self):
		pass
		
	@staticmethod
	def renderOverlays():
		for overlay in ScreenOverlay.overlays:
			overlay.render()


mouse = pymouse.PyMouse()
def moveMouse(x, y):
	mouse.move(int(x), int(y))
	
def centerMouse():
	rect = getScreenRectangle()
	moveMouse(rect[2]/2, rect[3]/2)

def getMousePosition():
	return mouse.position()
