from random import randint, random
import copy

import math
from SimpleAppFramework import *

import experiment_logger

COLORS = {
	"START": (255, 255, 255),
	"STOP": (255, 255, 255)
}

'''
	A PointAndClickTrial represents a clicking task with a series of targets.
	Clicking on the first target starts a stopwatch. Clicking on the last stops it.

	@param	*targets	the list of targets to be clicked on
'''
class PointAndClickTrial(Scenario):
	hasHadFirstRender = False
	def __init__(self, targets, recenterMouse=True):
		self.init()
		
		self.missedSelections = 0
		self.currentTargetID = 0
		
		self.targets = targets
		self.stopwatch = Stopwatch()
		self.recenterMouse = recenterMouse
		
		self.targetsFirstRender = True

	def getCurrentTarget(self):
		return self.targets[self.currentTargetID]

	def render(self):
		global screen 
		
		target = self.getCurrentTarget()
		target.render()
		Circle(target.pos[0], target.pos[1], 7, (128, 128, 128)).render()

		if not self.hasHadFirstRender:
			self.stopwatch.start()
			self.startTimeoutClock()
			self.hasHadFirstRender = True
			
		if self.targetsFirstRender:
			experiment_logger.logEvent("target-render", (target,))
			self.targetsFirstRender = False
		

	def onMouseDown(self, event):
		if self.hasHadFirstRender:
			if self.getCurrentTarget().contains(event.pos):
				experiment_logger.logEvent("HIT")
				playAudio(basedir + "/audio/click.wav")
				
				if self.currentTargetID == len(self.targets) - 1:
					self.stopwatch.stop()
					self.done = True
					if self.recenterMouse:
						centerMouse()
				else:
					self.stopwatch.lap()
					
				self.startTimeoutClock()
				self.currentTargetID += 1
				self.targetsFirstRender = True
			else:
				experiment_logger.logEvent("MISS")
				playAudio(basedir + "/audio/whiff.wav")
				self.missedSelections += 1
			
	def getClickToClickTime(self):
		if self.stopwatch.isRunning():
			return None
		else:
			return sum(self.stopwatch.laps[1:])

	def getTotalTime(self):
		if self.stopwatch.isRunning():
			return None
		else:
			return self.stopwatch.getTotalTime()

	def onTimeout(self):
		self.done = True
		showTimedMessage(2, "You took too long!")


'''
	The TwoTargetFactory generates PointAndClickTrial objects.
	It will ensure that both targets are on screen.
	The first target will always be white, with a size of 10
	The second target will always be cyan

	@param	targetDistance	How far the second target should be from the first
	@param	targetWidth	The size of the second target
'''
class TwoTargetFactory(object):
	def __init__(self, centerStartTarget=False, recenterMouse=False, startTargetSize=40):
		self.centerStartTarget = centerStartTarget
		self.recenterMouse = recenterMouse
		self.startTargetSize = startTargetSize

	def getLastAngle(self):
		return self.lastAngle

	def createTrial(self, targetDistance, targetWidth):
		global COLORS
		
		screenRectangle = getScreenRectangle()

		stopPos = None
		while stopPos == None:
			print "Generating start pos..."
			if self.centerStartTarget:
				startPos = (
					screenRectangle[2]/2 + self.startTargetSize,
					screenRectangle[3]/2 + self.startTargetSize
				)
			else:
				startPos = (
					randint(self.startTargetSize, screenRectangle[2]-self.startTargetSize),
					randint(self.startTargetSize + 30, screenRectangle[3]-self.startTargetSize - 60)
				)
			
			# generate a random angle
			angle = random() * 2 * math.pi

			# adjust the angle by 45 degrees until it fits inside the screen
			print "Generating stop pos..."
			for i in range(8):
				angle = angle + (i * math.pi / 4)
				if angle > 2 * math.pi:
					angle -= 2 * math.pi
					
				testPos = (
					startPos[0] + targetDistance * math.cos(angle),
					startPos[1] + targetDistance * math.sin(angle)
				)
				
				usableSpace = Rectangle(
					targetWidth,
					targetWidth + 30,	# text on the top
					screenRectangle[2] - targetWidth * 2,
					screenRectangle[3] - targetWidth * 2 - 60 # text on the bottom
				)

				if usableSpace.contains(testPos):
					stopPos = testPos
					break
					
				print "\tAdjusting stop pos...", testPos, targetDistance, targetWidth, angle
		print "Targets acquired"
		self.lastAngle = angle
		# Create the trial
		return PointAndClickTrial(
			[
				Circle(startPos[0], startPos[1], self.startTargetSize, color=COLORS["START"]),
				Circle(stopPos[0], stopPos[1], targetWidth, color=COLORS["STOP"], strokeWidth=2)
			], self.recenterMouse
		)

'''
'''
def calculateScore(distance, size, reactionTime, showMessage=False, playSound=False):
	smallDotMultiplier = 2
	farDistanceMultiplier = .5
	megaMultiplier = 3 #Small dot and far distance

	if reactionTime == None:
		return 0
		
	basePoints = 10000 - reactionTime * 1000
	bonus = 0
	if size == 40 and distance == 750 and random() <= .2: #Multiplier for small dots and far distance
		bonus = int(basePoints * megaMultiplier)
		if playSound: playAudio(basedir + "/audio/lowHighTone.wav")
	elif size == 40 and random() <= .3:
		bonus = int(basePoints * smallDotMultiplier)
		if playSound: playAudio(basedir + "/audio/lowTone.wav")
	elif distance == 750 and random() <= .3:
		bonus = int(basePoints * farDistanceMultiplier)
		if playSound: playAudio(basedir + "/audio/highTone.wav")

	if bonus > 0 and showMessage:
		showTimedMessage(1.5, "+ %d Bonus Points" % bonus)

	return basePoints + bonus


class HUD(ScreenOverlay):
	def __init__(self):
		self.overallScore = 0
		self.blockScore = 0
		self.progress = 0.0
		
	def setScores(self, totalScore, blockScore):
		self.overallScore = totalScore
		self.blockScore = blockScore
		
	def addScore(self, score):
		self.overallScore += score
		self.blockScore += score
		
	def setProgress(self, progress):
		self.progress = progress

	def render(self):
		rect = getScreenRectangle()

		renderText("Total Score: %d" % self.overallScore, rect[2]-300, 0)
		renderText("Block Score: %d" % self.blockScore, 10, 0)
		
		wrapText("Progress: %d%%" % int(100*self.progress), 0, rect[3]-30, align="center")
