# -*- coding: utf-8 -*-

from SimpleAppFramework import *
import socket, threading, time

import experiment_logger

class CursorStateReceiver(ScreenOverlay):
	dwell = []
	socket = None
	dwellTime = None
	dwellToSelectTime = None
	
	def __init__(self, host='localhost', port=1234):
		try:
			self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.socket.connect((host, port))
			self.socket.setblocking(0)
			self.lineReader = self.createLineReader()
			experiment_logger.logEvent("cursor-state", ["connected"])
		except:
			print "Could not connect to cursor state broadcaster"
			self.socket = None
		
	def render(self):
		if self.socket == None: return
		
		states = self.readLines()
		if len(states) > 0:
			state = states[-1].split("\t") # we only care about the most recent event
			experiment_logger.logEvent("cursor-state-update", state)

			if state[0] == 'd':
				self.dwellTime = time.time()
				self.dwell = [
					Circle(int(state[1]), int(state[2]), 9, (255, 0, 0), 1),
					Circle(int(state[1]), int(state[2]), 1, (255, 0, 0), 1),
				]
			elif state[0] == 'cd' or state[0] == 'c':
				self.dwell = []
				if state[0] == 'c':
					self.dwellToSelectTime = time.time() - self.dwellTime
			else:
				print "unknown state: '%s'" % state[0]
				
		for obj in self.dwell:
			obj.render()
			
	def getDwellToSelectTime(self):
		return self.dwellToSelectTime
		
	def readLines(self):
		lines = []
		line = ""
		while line != None:
			line = next(self.lineReader)
			if line != None:
				lines.append(line)
		
		return lines
	
	def createLineReader(self):
		buffer = ""
		while True:
			try:
				buffer += self.socket.recv(4096)
			except:
				yield None

			if "\n" in buffer:
				(line, buffer) = buffer.split("\n", 1)
				yield line
		
if __name__ == '__main__':
	import time
	
	rx = CursorStateReceiver()
	while True:
		state = rx.getNewState()
		if state != None:
			print state
			
