# -*- mode: python -*-

def globEm(fileExpr, matchExpr):
	from fnmatch import fnmatch
	
	g = []
	for path, dirs, files in os.walk(fileExpr):
		for file in files:
			if fnmatch(file, matchExpr):
				g.append(os.path.join(path, file))

	d = ['DATA'] * len(g)
	return zip(g, g, d) 


a = Analysis(
    ['main.py'],
    pathex=['.'],
    hiddenimports=[],
    hookspath=None
)
pyz = PYZ(a.pure)
exe = EXE(
    pyz,
    a.scripts,
    a.binaries,
    a.zipfiles,
    a.datas,
    globEm("stimulationLists", "*.txt"),
    globEm("audio", "*.wav"),
    name=os.path.join('dist', 'PointAndSelect.exe'),
    debug=False,
    strip=None,
    upx=True,
    console=False
#    icon='graphics\\icon.ico'
)
